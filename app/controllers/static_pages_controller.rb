class StaticPagesController < ApplicationController

  def about
  end

  def blog
  end

  def legal
  end
end
