class VotesController < ApplicationController
  before_action :authenticate_user!
  def create
    @picture = Picture.find(vote_params[:picture_id])
    vote = @picture.votes.where(:user_id => current_user.id).first
    if !vote
      @picture.vote(vote_params[:up] == "1", current_user.id)
    else
      @picture.change_vote(vote, vote.up, vote_params[:up] == "1")
    end
    @vote = vote_params[:up]
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

  private

    def vote_params
      params.require(:vote).permit(:up, :picture_id)
    end
end
