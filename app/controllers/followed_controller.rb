class FollowedController < ApplicationController
  before_action :authenticate_user!

  def index
    if confirm_params_or_redirect(params)
      redirect_to followed_index_path(:filter => session[:filter]) and return
    end
    @pictures = Picture.followed_by(current_user).filter(params.fetch(:filter, "hot")).paginate(:per_page => 20, :page => params[:page])
    @view = "followed"
    @followed_items = current_user.following.sort { |item| item.followers.count }[0..9]
    render 'pictures/index'
  end


end
