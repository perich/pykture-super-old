class SearchController < ApplicationController
  def search
    @tags = Tag.where("LOWER(name) LIKE LOWER(?)", "%" + params["srch-term"] + "%")
    @users = User.where("LOWER(username) LIKE LOWER(?)", "%" + params["srch-term"] + "%")
    @results = @tags + @users
    @json = @results.map { |item| {name: item.name, link: url_for(item)} }
    respond_to do |format|
      format.html
      format.json { render json: @json }
    end
  end
end
