class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_devise_params, if: :devise_controller?
  before_filter :get_background_picture

  protected

  def get_background_picture
    @background_picture = Rails.cache.fetch("background_picture", :expires_in => 30.seconds) do
      Picture.get_background_pic.picture.url
    end
  end

  def configure_devise_params
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
  end

  def confirm_params_or_redirect(params)
    redirect = false

    if params[:filter] and %w(hot popular recent liked).include? params[:filter][0]
      if %w(popular liked).include? params[:filter][0]
        if %w(day hour month all).include? params[:filter][1]
          session[:filter] = params[:filter]
        else
          session[:filter]
          redirect = true
        end
      else
        session[:filter] = params[:filter]
      end
    elsif session[:filter]
      session[:filter]
      redirect = true
    else
      session[:filter] = ["hot"]
      redirect = true
    end

    return redirect

  end
end
