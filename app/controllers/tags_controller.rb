class TagsController < ApplicationController

  def show
    @tag = Tag.find_by_name(params[:id])
    @filter = params.fetch(:filter, ["hot"])
    @pictures = @tag.pictures.filter(@filter).paginate(:per_page => 20, :page => params[:page])
  end

end
