class RelationshipsController < ApplicationController
	before_action :authenticate_user!, only: [:create, :destroy]

	def create
		@item = Object.const_get(params[:followable_type]).find(params[:followable_id])
		current_user.follow(@item)
		respond_to do |format|
			format.html { redirect_to @item }
			format.js
		end
	end

	def destroy
		@item = Object.const_get(params[:followable_type]).find(params[:followable_id])
		current_user.unfollow(@item)
		respond_to do |format|
			format.html { redirect_to :back }
			format.js
		end
	end
end
