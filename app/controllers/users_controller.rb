class UsersController < ApplicationController
  def show
    @user = User.find_by_username(params[:id])
    @filter = params.fetch(:filter, ["hot"])
    @pictures = @user.pictures.filter(@filter).paginate(:per_page => 20, :page => params[:page])
  end
end
