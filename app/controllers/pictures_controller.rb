class PicturesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]

  def index
    if confirm_params_or_redirect(params)
      redirect_to root_path(:filter => session[:filter]) and return
    end
    @pictures = Picture.filter(params.fetch(:filter, "hot")).paginate(:per_page => 20, :page => params[:page])
    @view = "all"
  end

  def create
    @picture = Picture.create(picture_params)

    respond_to do |format|
      if @picture.save
        format.html {
          render :json => [@picture.to_jq_upload].to_json,
          :content_type => 'text/html',
          :layout => false
        }
        format.json { render json: {files: [@picture.to_jq_upload]}, status: :created, location: @upload }
      else
        format.html { render "new" }
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @picture = Picture.find(params[:id])
    if @picture.user = current_user
      @picture.destroy
    end
    @failed = Picture.exists?(@picture)
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end



  

  private

    def picture_params
      params.require(:picture).permit(:picture, :blurb).merge(:user_id => current_user.id)
    end
end
