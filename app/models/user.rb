class User < ActiveRecord::Base
  has_many :followers, class_name: "Relationship", :as => :followable
  has_many :followed_items, class_name: "Relationship", :foreign_key => :follower_id
  has_many :followed_users, :through => :followed_items, :source => :followable, :source_type => "User"
  has_many :followed_tags, :through => :followed_items, :source => :followable, :source_type => "Tag"
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :recoverable
  devise :database_authenticatable, :registerable,
       :rememberable, :trackable, :validatable
  has_many :votes, :dependent => :destroy
  has_many :pictures                              
  validates :username, :presence => true, length: { in: 3..22 }, uniqueness: true
  validates :username, format: { with: /\A[a-zA-Z0-9]+\Z/, :message => "can't have spaces, only a-z A-Z 0-9" }
  validates :password, :presence => true, length: { minimum: 6 }
  validates :password_confirmation, :presence => true


  def email_required?
    false
  end

  def to_param
    username
  end

  def name
    username
  end

  def following
    self.followed_users + self.followed_tags
  end

  def voted_on?(picture)
    picture.votes.where(:user_id => self.id).first.up rescue 'no vote'
  end

  def followed_users_ids
    self.followed_users.map { |user| user.id }
  end

  def followed_tags_ids
    self.followed_tags.map { |tag| tag.id }
  end

  def follow(followable)
    followed_items.create(:followable => followable)
  end

  def unfollow(followable)
    followed_items.find_by(:followable => followable).destroy
  end

  def following?(followable)
    following.include?(followable)
  end

  def self.top_five
    User.select("users.*, count(relationships.followable_id) as count").joins(:followers).group("users.id").order("count DESC").limit(5)
  end


end
