class Picture < ActiveRecord::Base
  before_save :create_tags
  mount_uploader :picture, PictureUploader

  has_many :pictures_tags, :dependent => :destroy
  has_many :tags, through: :pictures_tags 

  has_many :votes, :dependent => :destroy
  belongs_to :user
  validates :picture, :presence => true
  require 'will_paginate/array'

  scope :filter, lambda { |params|
    case params[0]
      when "hot"
        includes(:tags, :user, :votes)
        .sort_by(&:hot_number)
        .reverse!
      when "recent"
        order('created_at DESC')
        .includes(:tags, :user, :votes)
      when "popular"
        if %w(day hour month year).include?(params[1])
          where('created_at > ?', 1.send(params[1]).ago)
        else
          all
        end
        .order('number_of_votes DESC, number_of_upvotes DESC')
        .includes(:tags, :user, :votes)
      when "liked"
        if %w(day hour month year).include?(params[1])
          where('created_at > ?', 1.send(params[1]).ago)
        else
          all
        end
        .order('number_of_upvotes DESC, number_of_votes ASC')
        .includes(:tags, :user, :votes)
      else # also HOT
        includes(:tags, :user, :votes)
        .sort_by(&:hot_number)
        .reverse!
      end
    } 
  # ------------
  # Filters
  #-------------

  def self.followed_by(user)
    Picture.joins(:pictures_tags)
    .where('pictures.user_id IN (?) OR pictures_tags.tag_id IN (?)', user.followed_users_ids, user.followed_tags_ids )
    .uniq
  end

  # ------------
  # Voting
  #-------------

  def vote(direction, user_id)
    if direction
      self.number_of_votes += 1
      self.number_of_upvotes += 1
      self.votes.create(:user_id => user_id,
              :picture_id => self.id,
              :up => true)
      self.save
    else
      self.number_of_votes += 1
      self.votes.create(:user_id => user_id,
              :picture_id => self.id,
              :up => false)
      self.save
    end 
    return self
  end

  def change_vote(vote, previous_direction, new_direction)
    if previous_direction == new_direction
      return self
    elsif new_direction == true
      vote.up = true
      self.number_of_upvotes += 1
    else
      vote.up = false
      self.number_of_upvotes -= 1
    end
    vote.save
    self.save
    return self
  end
  

  def up_votes
    # return self.votes.where('up like ?', true).count.to_f
    return self.number_of_upvotes
  end

  def self.get_background_pic
    where('created_at > ?', 1.day.ago).order('number_of_upvotes DESC, number_of_votes ASC').sample.presence || last
  end

  def hot_number
    return (up_votes - 1) / (hours_ago + 2)**1.5
  end

  def hours_ago
    ((Time.now - created_at)/60/60)
  end

  # ------------
  # Tagging
  #-------------

  def self.tagged_with(name)
    Tag.find_by_name!(name).pictures
  end

  def self.tag_counts
  Tag.select("tags.*, count(pictures_tags.tag_id) as count").joins(:pictures_tags).group("tags.id").order("count DESC").limit(10)
  end

  def tag_list
    tags.map(&:name).join(", ")
  end

  def tag_list=(names)
    self.tags = names.map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end

  include Rails.application.routes.url_helpers

  def to_jq_upload
    {
      "name" => self.blurb,
      "size" => self.picture.size,
      "url" => self.picture.url,
      "delete_url" => picture_path(self),
      "delete_type" => "DELETE"
    }
  end

  private

  # Adds existing tags to Picture or creates new Tags if they dont exist
  def create_tags
    hashtag_regex = /\B#\w\w+/
    tags = self.blurb.scan(hashtag_regex)
    tags.each do |tag|
      to_add = Tag.where(:name => tag).first_or_create!
      self.tags << to_add unless self.tags.include?(to_add)
    end
  end




end
