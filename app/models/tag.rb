class Tag < ActiveRecord::Base
	has_many :pictures_tags
	has_many :pictures, through: :pictures_tags
  has_many :followers, class_name: "Relationship", :as => :followable

  def self.top_five
    Tag.select("tags.*, count(relationships.followable_id) as count").joins(:followers).group("tags.id").order("count DESC").limit(5)
  end

  def to_param
    name
  end
end
