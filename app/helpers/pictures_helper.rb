module PicturesHelper

	def with_links(picture)
		blurb = picture.blurb
		picture.tags.each do |tag|
			blurb = blurb.sub tag.name, link_to(tag.name, tag_path(tag.name))
		end
		raw blurb
	end
end
