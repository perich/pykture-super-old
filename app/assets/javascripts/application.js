// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-fileupload
//= require bootstrap-sprockets
//= require jquery.colorbox
//= require typeahead
//= require_tree .


var ready = function() {
	var css = '<style>body::before{background: url("' + background_picture + '") no-repeat center fixed;-webkit-background-size:cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;}';
	$('head').append(css);

	// Picture voting/blurb
	// $(".pop").show()
	$('.single_picture').hover(function(){
		$(this).children('.pop').slideDown(75);
	}, function(){
		$(this).children('.pop').slideUp(75);
	});

	// File Upload form
	$(".picture_form").hide();
	$('#upload_form').click(function(){
		$(".picture_form").slideToggle();
	});

	// Colorbox
	$('a#full_size').colorbox({
		// html: $(this).html(),
		rel: "gallery",
		maxHeight: "100%",
		maxWidth: "100%",
		current: false,
		// title: $('img.cboxPhoto').attr("alt"),
		title: function(){
			// console.log($(this).siblings().find('.voters').html());
			return $(this).siblings().find('.voters').html();
		},
		photo: true
		// current: false
	});

	// Search 
	var SearchResults = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  remote: '../search?srch-term=%QUERY'
	});
	 
	SearchResults.initialize();


	$('.searcher .typeahead').typeahead(null, {
	  displayKey: 'name',
	  source: SearchResults.ttAdapter(),
	  templates: {

	  }
	});

	$('.searcher .typeahead').bind('typeahead:selected', function(obj, datum, name) {      
	        window.location.href = datum.link;
	});




};
$(document).ready(ready);

var modalshow = function(){
  var width,height,padding,top,left,modalbak,modalwin;
  width   = 700;
  height  = 425;
  padding = 64;
  top     = (window.innerHeight-height-padding)/2;
  left    = (window.innerWidth-width-padding)/2;

  modalbak = document.getElementById("modalbak");
  modalbak.style.display = "block";

  modalwin = document.getElementById("modalwin");
  modalwin.style.top     = top+"px";
  modalwin.style.left    = left+"px";
  modalwin.style.display = "block";
}
var modalhide = function(){
  document.getElementById("modalbak").style.display = "none";
  document.getElementById("modalwin").style.display = "none";
}


