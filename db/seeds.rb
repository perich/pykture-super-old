# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

session = {}
# User.destroy_all
# Picture.destroy_all
# Tag.destroy_all
# Picture.destroy_all

clem = User.create(username: "ClemFandango", password: "password", password_confirmation: "password")
guy = User.create(username: "ThePictureGuy", password: "password", password_confirmation: "password")
gravitas = User.create(username: 'gravitas', password: "password", password_confirmation: "password")

users = [clem, guy, gravitas]

fake_users = Array.new(20) do
  User.create(username: Faker::Internet.user_name, password: "password", password_confirmation: "password")
end
# Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/motivation.jpg", "r"),
# 					:blurb => "this is a #test #tag #one", :user_id => perich.id).save
# Picture.new(:picture => File.new("/Users/pe_rich/Desktop/jjanny.jpg", "r"),
# 					:blurb => "this is a #test #jjanny #park", :user_id => perich.id).save
# Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/syd_barrett.jpg", "r"),
# 					:blurb => "this is a #test #blurb", :user_id => perich.id).save
# Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/screen_golf.jpg", "r"),
# 					:blurb => "this is a #test #blurb", :user_id => perich.id).save

# Clem/home/deployer/pykture/pyktureseeds/pyktureseeds/  /Users/pe_rich/Desktop/WebProgramming/pykture/pyktureseeds/
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/bridge.jpeg", "r"),
					:blurb => "Fun trip to #NYC", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/cameras.jpeg", "r"),
					:blurb => "Some #old #cameras I found", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/dune.jpeg", "r"),
					:blurb => "I want to #hike on these #dunes", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/forest.jpeg", "r"),
					:blurb => "Went for a #hike in the #woods", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/mac.jpg", "r"),
          :blurb => " ", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/seacliffs.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/saltycliff.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/goldengate.jpg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/sunset.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/waterfall.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/road.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/cave.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/snow.jpeg", "r"),
          :blurb => "", :user_id => clem.id).save

# Guy
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/nyc.jpeg", "r"),
					:blurb => "Another weekend in #NYC", :user_id => guy.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/old_truck.jpeg", "r"),
					:blurb => "grandpas #old #truck", :user_id => guy.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/shore.jpeg", "r"),
					:blurb => "#weekend at the #beach", :user_id => guy.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/snowyroad.jpeg", "r"),
					:blurb => "going away for the #weekend", :user_id => guy.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/trail.jpeg", "r"),
          :blurb => "", :user_id => guy.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/clouds.jpeg", "r"),
          :blurb => "", :user_id => guy.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/trees.jpeg", "r"),
          :blurb => "", :user_id => guy.id).save

# Gravitas
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/bostonport.jpg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/girlonbridge.jpg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/bubble.jpg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/hotspring.jpeg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/plane.jpeg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/poolwaves.jpeg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/shootingstar.jpeg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/stars.jpg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/river.jpeg", "r"),
           :blurb => "", :user_id => gravitas.id).save
Picture.new(:picture => File.new("/home/deployer/pykture/pyktureseeds/pyktureseeds/snowyvalley.jpeg", "r"),
           :blurb => "", :user_id => gravitas.id).save


150.times do
  Picture.all.sample.vote(true, fake_users.sample.id)
end



# # Ranking testers

# # picture12 = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/1.gif", "r"),
# # 					:blurb => "#ranking", :user_id => clem.id)
# # picture12.save
# # picture12.created_at -= 2.months
# # picture12.number_of_upvotes = 1122932
# # picture12.number_of_votes = 1122932
# # picture12.save

# # picture13 = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/2.gif", "r"),
# # 					:blurb => "#ranking", :user_id => clem.id)
# # picture13.save
# # picture13.created_at -= 5.days
# # picture13.number_of_upvotes = 25697
# # picture13.number_of_votes = 25697
# # picture13.save

# # picture14 = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/3.gif", "r"),
# # 					:blurb => "#ranking", :user_id => clem.id)
# # picture14.save
# # picture14.created_at -= 1.day
# # picture14.number_of_upvotes = 2427
# # picture14.number_of_votes = 2427
# # picture14.save

# # picture15 = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/4.gif", "r"),
# # 					:blurb => "#ranking", :user_id => clem.id)
# # picture15.save
# # picture15.created_at -= 6.hours
# # picture15.number_of_upvotes = 407
# # picture15.number_of_votes = 407
# # picture15.save

# # picture16 = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/lorem_images/5.gif", "r"),
# # 					:blurb => "#ranking", :user_id => clem.id)
# # picture16.number_of_upvotes = 47
# # picture16.number_of_votes = 47
# # picture16.save



# # jdoe.follow(:followable => Tag.first)
# # jdoe.follow(:followable => Tag.last)
# # jdoe.follow(:followable => Tag.last)
# # jdoe.follow(:followable => Tag.last)




