class AddVotingToPictures < ActiveRecord::Migration
  def change
  	add_column :pictures, :votes, :integer, :default => 0
  	add_column :pictures, :rating, :integer, :default => 0
  end
end
