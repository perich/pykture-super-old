class ChangeModelVotesAndRatingNamesForPicture < ActiveRecord::Migration
  def change
  	rename_column :pictures, :votes, :number_of_votes
  	rename_column :pictures, :rating, :number_of_upvotes
  end
end
