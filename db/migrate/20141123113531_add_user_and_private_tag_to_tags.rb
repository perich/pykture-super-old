class AddUserAndPrivateTagToTags < ActiveRecord::Migration
  def change
    add_column :tags, :private_tag, :boolean, :default => false
    add_column :tags, :user_tag, :boolean, :default => false
  end
end
