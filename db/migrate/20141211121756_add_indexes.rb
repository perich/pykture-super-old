class AddIndexes < ActiveRecord::Migration
  def change
    add_index :pictures, :user_id
    add_index :pictures_tags, :picture_id
    add_index :pictures_tags, :tag_id
    add_index :votes, :picture_id
    add_index :votes, :user_id
  end
end
