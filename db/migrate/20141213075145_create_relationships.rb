class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :follower_id
      t.integer :followable_id
      t.string :followable_type

      t.timestamps
    end
    add_index :relationships, [:followable_type, :followable_id]
  end
end
