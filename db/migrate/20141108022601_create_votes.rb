class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :user_id
      t.integer :picture_id
      t.boolean :up, :default => false

      t.timestamps
    end
  end
end
