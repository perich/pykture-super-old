
require 'rails_helper'

describe User do

	before do
		@user = User.new(username: "Example", email: "user@example.com", password: "foobar", password_confirmation: "foobar")
	end

	subject { @user }

	it { should respond_to(:username) }
	it { should respond_to(:email) }
	it { should respond_to(:password) }
	it { should respond_to(:password_confirmation) }
	it { should respond_to(:pictures) }
	it { should respond_to(:votes) }

	describe "username: " do
		it "should not be valid without" do
			@user.username = nil
			@user.should_not be_valid
		end
		it "should not be less that 3 characters" do
			@user.username = "aa"
			@user.should_not be_valid
		end
		it "should not be longer than 50 characters" do
			@user.username = ("a" * 51)
			@user.should_not be_valid
		end
		it "should not already be taken" do
			@user.username = "test"
			@user.save
			user2 = User.new(:username => "test")
			user2.should_not be_valid
		end
		it "should save" do
			@user.save
			@user.should be_valid
			@user.username.should == "Example"
		end
	end

	describe "email: " do
		it "should be valid without email" do
			@user.email = nil
			@user.should be_valid
		end
	end

	describe "password: " do
			it "should validate presence of" do
				@user.password = nil
				@user.should_not be_valid
			end
			it "should be longer than 5 characters" do
				@user.password = 'fooba'
				@user.password_confirmation = 'fooba'
				@user.should_not be_valid
			end
			it "should not be longer than 128 characters" do
				@user.password = ('f' * 129)
				@user.password_confirmation = ('f' * 129)
				@user.should_not be_valid
			end
			it "should have match password_confirmation" do
				@user.password_confirmation = "other"
				@user.should_not be_valid
			end
	end


end