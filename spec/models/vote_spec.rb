require 'rails_helper'

describe Vote do

	let!(:user) { User.create(username: "Example", email: "user@example.com", password: "foobar", password_confirmation: "foobar") }
	let!(:picture) { Picture.create(:picture => File.new("/Users/pe_rich/Desktop/photo.JPG", "r"), :blurb => "this is a #test #blurb") }

	describe "Upvote:" do
		before do 
			picture.vote(true, user.id)
		end
		it "should add one new Vote" do 
			subject { should.change(Vote, :count).by(1) }
		end
		it "should have :up equal true" do
			picture.votes.last.up.should == true
		end
		it "should change picture.votes.count by 1" do
			picture.votes.count.should == 1
		end
		it "should increase picture.number_of votes" do 
			picture.number_of_votes.should == 1
		end
		it "should increase picture.number_of_upvotes by 1" do
			picture.vote(true, user.id).number_of_upvotes { should == 1 }
		end
	end
	describe "Downvote:" do
		it "should add one new Vote" do
			picture.vote(false, user.id) { should change(Vote, :count).by(1) }
		end
		it "should have :up equal false" do
			picture.vote(false, user.id)
			picture.votes.last.up.should == false
		end
		it "should increase picture.number_of_votes" do
			picture.vote(false, user.id).number_of_votes.should == 1
		end
		it "should not increase picture.number_of_upvotes" do
			picture.vote(false, user.id).number_of_upvotes.should == 0 
		end
		describe "then downvote again" do
			xit "should not change number_of_votes"
			xit "should not change number_of_upvotes"
		end
		describe "then upvote" do
			xit "should change number_of_upvotes by 1"
		end
	end

end