require 'rails_helper'

describe Picture do

	before do
		@user = User.new(username: "Example", email: "user@example.com", password: "foobar", password_confirmation: "foobar")
		@picture = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/photo.JPG", "r"), :blurb => "this is a #test #blurb")
		@picture2 = Picture.new(:picture => File.new("/Users/pe_rich/Desktop/photo.JPG", "r"), :blurb => "this is a #test #blurb")
	end

	subject { @picture }
	let!(:tag) { Tag.new(:name => "#tagger") }

	it { should respond_to(:user) }
	it { should respond_to(:votes) }
	it { should respond_to(:tags) }
	it { should respond_to(:picture) }
	it { should respond_to(:blurb) }
	it { should respond_to(:up_votes) }
	it { should respond_to(:down_votes) }
	it { should respond_to(:vote_count) }
	it { should respond_to(:percent) }
	it { should respond_to(:tag_list) }
	it { should respond_to(:vote) }
	it { should respond_to(:number_of_votes) }
	it { should respond_to(:number_of_upvotes) }

	it "should require a picture" do
		picture = Picture.new(:blurb => "this is a #test #blurb")
		picture.should_not be_valid
	end

	describe "blurb" do 
		it "should have tags if blurb has any" do
			@picture.blurb = "#hash"
			@picture.save
			@picture.tags.count.should == 1
		end
		it "should not create/have tags if no # in blurb" do
			@picture.blurb = "test"
			@picture.save
			@picture.tags.count.should == 0
		end
		it "should not create new tag for existing tags" do
			tag.save
			@picture.blurb = "#tagger"
			@picture.save
			@picture.tags.first.id == tag.id
		end
	end

	describe "tag.pictures.count" do 
		before do
			@picture.blurb = "#tagger"
		end
		it "should increase when new picture with #tag created" do
			@picture.save { should change(tag, :count).by(1) }
		end
		it "should decrease when picture with #tag destroyed" do
			@picture.save
			@picture.destroy { should change(tag, :count).by(-1) }
		end
	end
end







